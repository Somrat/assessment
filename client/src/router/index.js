import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from "@/views/Login";
import Dashboard from "@/views/Dashboard";
import store from '@/store'
import Books from "@/views/Books";
import Registration from "../views/Registration";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
            requiresAuth: false,
            is_admin: false
        }
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
        meta: {
            requiresAuth: false,
            is_admin: false
        }
    },
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard,
        meta: {
            requiresAuth: true,
            is_admin: false
        }
    },

    {
        path: '/book-create',
        name: 'Books',
        component: Books,
        meta: {
            requiresAuth: true,
            is_admin: false
        }
    },
    {
        path: '/user-registration',
        name: 'Registration',
        component: Registration,
        meta: {
            requiresAuth: true,
            is_admin: true
        }
    }


]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (localStorage.getItem('token') == null) {
            next({
                path: '/login',
                params: { nextUrl: to.fullPath }
            })
        } else {
            let user = store.getters['auth/user']
            if (to.matched.some(record => record.meta.is_admin)) {
                if (user.user_role == 1) {
                    next()
                } else {
                    next({ name: 'Dashboard' })
                }
            } else {
                next()
            }
        }
    } else {
        next()
    }
})

export default router
