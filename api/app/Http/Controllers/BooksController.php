<?php

namespace App\Http\Controllers;

use App\Models\Books;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BooksController extends Controller
{
    public function bookCreate(Request $request)
    {
    
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'writer_name' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        try {
            $data = Books::create([
                'name' => $request->get('name'),
                'writer_name' => $request->get('writer_name')
            ]);
        }
        catch (\Exception $e)
        {
            $data='data not insert';
        }
        return response()->json([
                'success' => true,
                'message' => 'data insert successfully ',
                'data'=> $data
            ], 200);


    }


     public function destroy($id)
    {
        $book = Books::find($id);
    
        if (!$book) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, book with id ' . $id . ' cannot be found'
            ], 400);
        }
    
        if ($book->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'book could not be deleted'
            ], 500);
        }
    }


    public function show($id)
    {
        $book = Books::find($id);
    
        if (!$book) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, book with id ' . $id . ' cannot be found'
            ], 400);
        }
    
        return $book;
    }




}
