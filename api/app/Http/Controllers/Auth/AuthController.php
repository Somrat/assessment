<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Utils;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails())
            return response($validator->errors(), 404);


        try {
            if (!$token = auth()->attempt($request->only('email', 'password'))) {
                return response()->json(['error' => 'invalid credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }


        /*  if (!$token = auth()->attempt($request->only('email', 'password'))) {
              return response()->json(['message' => 'Invalid'], 401);
          }*/

        return response()->json(compact('token'));
    }

    public function logout(Request $request)
    {
        try {
            auth()->logout();

            return response()->json([
                'success' => true,
                'message' => 'User logged out successfully'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, the user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function profile(Request $request)
    {
        $user = $request->user();

        return response()->json([
            'name'      => $user->name,
            'email'     => $user->email,
            'user_role' => $user->user_role
        ]);
    }
}
