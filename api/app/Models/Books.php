<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    protected $table = 'books';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'writer_name', 'created_by','updated_by','created_at','updated_at'
    ];
}
