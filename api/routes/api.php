<?php

use App\Http\Controllers\Auth\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthorsController;
use App\Http\Controllers\BooksController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [AuthController::class, 'login']);

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('profile', [AuthController::class, 'profile']);

    Route::post('book_store', [BooksController::class,'bookCreate']);
    Route::get('book-show/{id}', [BooksController::class,'show']);
    Route::get('book-destroy/{id}', [BooksController::class,'destroy']);


    Route::post('logout', [AuthController::class, 'logout']);
});


Route::group(['middleware' => ['auth:api','admin']], function() {
    Route::post('register', [AuthorsController::class,'register']);
});
