<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@email.com',
            'user_role' => '1',
            'password' => Hash::make('123456'),
        ]);
        DB::table('users')->insert([
            'name' => 'User',
            'email' => 'user@email.com',
            'user_role' => '2',
            'password' => Hash::make('123456'),
        ]);
    }
}
